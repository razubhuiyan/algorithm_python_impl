def insertion_sort(A_list):

    for j in range(len(A_list)):

        key = A_list[j]
        i = j-1
        
        while i>=0 and A_list[i] > key:

            A_list[i+1] = A_list[i]
            i = i-1
        A_list[i+1] = key




def Main():

    A_list = [5, 2, 4, 6, 1, 3]

    insertion_sort(A_list)
    print("+++++++Sorted List+++++++")
    for j in range(len(A_list)):
        print(A_list[j])



if __name__ == "__main__":
    Main()